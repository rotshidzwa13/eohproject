package za.co.cyber.systems.tech.fleet.rest.xml;

import java.util.HashMap;
import java.util.Map;

public class CicService {
	
	private static CicService cicService = null;

	private Map<Long,Cic> IN_MEMORY = new HashMap<Long, Cic>();

	private CicService(){}
	
	public static CicService getService(){
		if(cicService == null){
			cicService = new CicService();
		}
		return cicService;
	}
	
	public boolean register(CicRegisterRequest request){
		
		Cic cic = new Cic();
		cic.setCicid(request.getCicid());
		cic.setCicTimestamp(request.getCicTimestamp());
		cic.setCicType(request.getCicType());
		cic.setSourceSystem(request.getSourceSystem());
		cic.setSubject(request.getSubject());
		
		Entity entity = new Entity();
		entity.setEntityId(request.getCicid());
		entity.setEmailAddress(request.getEmail());
		entity.setEntityName(request.getName());
		cic.setEntity(entity);
		
		add(cic);
		
		return true;
	}
	
	public int numberOfRecord(){
		return IN_MEMORY.size();
	}
	
	public Cic findByCicId(Long cicId){
		return IN_MEMORY.get(cicId);
	}
	
	private void add(Cic cic){
		IN_MEMORY.put(new Long(cic.getCicid()), cic);
	}
}
