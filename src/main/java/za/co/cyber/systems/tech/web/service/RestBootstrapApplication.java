package za.co.cyber.systems.tech.web.service;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.*;

@ApplicationPath("/rest")
public class RestBootstrapApplication extends Application{
	
	/*private Set<Object> singletons = new HashSet<>();
	
	public RestBootstrapApplication() {
		singletons.add(new GpsCoordService());

	}
	
	@Override
	public Set<Object> getSingletons() {	
		
		return singletons;
	}
	
	
	*/
	
	 private Set<Class<?>> resources = new HashSet<Class<?>>();

	    public RestBootstrapApplication () {
	        resources.add(TestService.class);
	    }

	    @Override
	    public Set<Class<?>> getClasses() {
	        return resources;
	    }
}