package za.co.cyber.systems.tech.fleet.rest.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "register_request")
public class CicRegisterRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long cicid;
	private String cicType;
	private String subject;
	private String sourceSystem;
	private String cicTimestamp;
	private String name;
	private String email;

	public long getCicid() {
		return cicid;
	}

	@XmlElement
	public void setCicid(long cicid) {
		this.cicid = cicid;
	}

	public String getCicType() {
		return cicType;
	}

	@XmlElement
	public void setCicType(String cicType) {
		this.cicType = cicType;
	}

	public String getSubject() {
		return subject;
	}

	@XmlElement
	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}

	@XmlElement
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getCicTimestamp() {
		return cicTimestamp;
	}

	@XmlElement
	public void setCicTimestamp(String cicTimestamp) {
		this.cicTimestamp = cicTimestamp;
	}

	public String getName() {
		return name;
	}

	@XmlElement
	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	@XmlElement
	public void setEmail(String email) {
		this.email = email;
	}

}
