/*package za.co.cyber.systems.tech.fleet.rest.xml;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "error_reponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ErrorResponse {

	@XmlElement
    private String name;
    @XmlElement
    private String message;
	@XmlElement
	private boolean valid;

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public boolean isValid() {
		return valid;
	}
	
	public void setValid(boolean valid) {
		this.valid = valid;
	}
}*/