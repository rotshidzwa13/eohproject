package za.co.cyber.systems.tech.fleet.rest.xml;

import java.util.Date;

public class Test {

	
	public static void main(String []args){
		CicService cic = CicService.getService();
		CicRegisterRequest request = new CicRegisterRequest();
		request.setCicid(1);
		request.setCicTimestamp(""+new Date());
		request.setCicType("MIME");
		request.setEmail("rotshi@home.co.za");
		request.setName("rotshi");
		request.setSourceSystem("dsa");
		request.setSubject("emergency");
		
		cic.register(request);
		System.out.println(""+cic.numberOfRecord());
		Cic c1 = cic.findByCicId(new Long(1));
		System.out.println(c1.getEntity().getEntityName());
	}
	
}
