package za.co.cyber.systems.tech.fleet.rest.xml;


public class Cic {
	private long cicid;
	private String cicType;
	private String subject;
	private String sourceSystem;
	private String cicTimestamp;
	private Entity entity;
	
	public Entity getEntity(){
		return entity;
	}
	
	public void setEntity(Entity entity){
		this.entity = entity;
	}

	public long getCicid() {
		return cicid;
	}
	
	public void setCicid(long cicid) {
		this.cicid = cicid;
	}

	public String getCicType() {
		return cicType;
	}
	public void setCicType(String cicType) {
		this.cicType = cicType;
	}

	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSourceSystem() {
		return sourceSystem;
	}
	
	public void setSourceSystem(String sourceSystem) {
		this.sourceSystem = sourceSystem;
	}

	public String getCicTimestamp() {
		return cicTimestamp;
	}
	
	public void setCicTimestamp(String cicTimestamp) {
		this.cicTimestamp = cicTimestamp;
	}
}
