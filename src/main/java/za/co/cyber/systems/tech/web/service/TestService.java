package za.co.cyber.systems.tech.web.service;

import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import za.co.cyber.systems.tech.fleet.rest.xml.Cic;
import za.co.cyber.systems.tech.fleet.rest.xml.CicRegisterRequest;
import za.co.cyber.systems.tech.fleet.rest.xml.CicRegisterResponse;
import za.co.cyber.systems.tech.fleet.rest.xml.CicResponse;
import za.co.cyber.systems.tech.fleet.rest.xml.CicService;

@Path("/test-service")
public class TestService {

	public TestService() {
	}

	@GET
	@Path("/ping")
	@Produces(MediaType.TEXT_PLAIN)
	public String ping() {

		String response = "Ping Response " + new Date();
		return response;
	}

	@POST
	@Path("/cic")
	@Consumes(MediaType.APPLICATION_XML)
	@Produces(MediaType.APPLICATION_XML)
	public Response register(CicRegisterRequest request) {
		boolean isRegistered = true;
		
		CicService cicService = CicService.getService();
		
		cicService.register(request);

		CicRegisterResponse response = new CicRegisterResponse();
		response.setRegistered(isRegistered);

		return Response.status(200).entity(response).build();

	}

	@GET
	@Path("/cic/{cidId}")
	@Produces("application/xml")
	public Response findCic(@PathParam("cidId") String cidId) {
        
		CicService cicService = CicService.getService();
		
		Cic cic = cicService.findByCicId(new Long(cidId));
		
		CicResponse response = new CicResponse();
		response.setCicid(cic.getCicid());
		response.setCicTimestamp(cic.getCicTimestamp());
		response.setCicType(cic.getCicType());
		response.setEmail(cic.getEntity().getEmailAddress());
		response.setName(cic.getEntity().getEntityName());
		response.setSourceSystem(cic.getSourceSystem());
		response.setSubject(cic.getSubject());
		
		return Response.status(200).entity(response).build();
	}

}
