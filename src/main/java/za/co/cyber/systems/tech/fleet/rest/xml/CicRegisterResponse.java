package za.co.cyber.systems.tech.fleet.rest.xml;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "register_response")
public class CicRegisterResponse implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private boolean registered;

	public boolean isRegistered() {
		return registered;
	}

	@XmlElement
	public void setRegistered(boolean registered) {
		this.registered = registered;
	}

	

}
